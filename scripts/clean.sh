python3 src/dvc_outs_remove.py --stage=sonicFoam --sim_dir=sonicFoam
python3 src/dvc_outs_remove.py --stage=postProcessing --sim_dir=sonicFoam                  
python3 src/dvc_outs_remove.py --stage=plotFloatPressure --sim_dir=sonicFoam
python3 src/dvc_outs_remove.py --stage=scalarTransportFoam --sim_dir=scalarTransportFoam
git clean -f -d
rm -rf .dvc/cache
rm -rf .dvc/tmp