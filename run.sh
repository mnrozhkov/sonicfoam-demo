#!/bin/bash


# If the user is root, its UID is zero.
echo "Run run.sh script"
if [ "$CI"  =  "true" ] ;
then
  echo "Run in CI pipeline"
  eval "$1"
else
  chmod +x openfoam-cse-docker 
  ./openfoam-cse-docker -c "$1"
fi