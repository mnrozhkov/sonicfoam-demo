# Getting started
This demo runs an OpenFOAM simulation pipeline with DVC simulation. The pipeline contains a few steps: 
- Generate geometry with `blockMesh`
- Run `sonicFoam` simulation to get velocity (U) and temperature (T) field
- Post-processing simulation results
- Run a subsequent `scalarTransportFoam` simulation that uses the velocity field computed before

Every step is running by separate command.

## Setup

### 1 - Build OpenFoam Docker image 
```
docker build -f docker/Dockerfile -t cse/openfoam-demo .
```

### 2 - Installation Python deps
In order to use the Python module you need the PyFoam package.
```bash
python3 -m venv .venv
echo "export PYTHONPATH=$PWD" >> .venv/bin/activate
source .venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt 
pip install -r requirements-dev.txt 
python -m ipykernel install --user --name=openfoam
``` 


### 3 - Setup DVC remote storage

Add remote storage for DEV (local dir): 
```bash
dvc remote modify local url PATH_TO_LOCAL_DIR_STORAGE 
``` 

Add remote storage in AWS S3 bucket: 
```bash
dvc remote modify s3remote url s3://YOUR_S3_BUCKET_URL
``` 
Refs: 
- [Example: Customize an additional S3 remote](https://dvc.org/doc/command-reference/remote#example-customize-an-additional-s3-remote)


## Run

### 1 - Run  with  DVC
To run a new simulation experiment, update config (`params.yaml`) and run:
```bash
dvc exp run
```

Change the simulation length and run a new one:
```bash
dvc exp run -S 'configureSim.controlDict.params.endTime=4'
```  



### 2 - Run OpenFoam in Docker
To run OpenFoam simulations in Docker, we use `openfoam-cse-docker` script. [Here is quick-start](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/docker) details on OpenFoam in Docker. 


Make `openfoam-cse-docker` script executable. 
```
chmod +x openfoam-cse-docker
```

To experiment with OpenFoam simulation in interactive way, run Docker containers and connect to the terminal session: 
```bash
./openfoam-cse-docker
```

Get options for the entry point within the image:
```bash
./openfoam-cse-docker / -help
```


### 3 - Run experiment via GitLab CI or Iterative Studio 

**Set CI variables** 
To run simulations in AWS with GitLab CI & CML, add the following CI variables in the project `  `Settings  → CI/CD → Variables` : 
```
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_SESSION_TOKEN
- REPO_TOKEN - personal GitLab access token 
```

**Run in Iterative Studio** 
- use Iterative Studio: select experiments -> Run -> update params -> Commit changes
- update configuration in `params.yaml` and push to Git repo


##References
- [OpenFOAM v9 User Guide - 6.2 Post-processing command line interface (CLI)](https://doc.cfd.direct/openfoam/user-guide-v9/post-processing-cli)
- [Visualizing CFD data using plotly](https://brelje.net/blog/visualizing-cfd-data-plotly/)
-  [Running OpenFOAM in a Docker container](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/docker#running-openfoam-in-a-container)
