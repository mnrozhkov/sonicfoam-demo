import argparse
from dataclasses import dataclass, field
# from dvc.repo import Repo
import git
from invoke import Context
import os
from pathlib import Path
import re
import shutil
from typing import Any, Text


def dvc_remove(outs_dir) -> Any:

    c = Context()
    
    files = "  ".join([f"\"{file}\"" for file in outs_dir.rglob("*.dvc")])
    
    if len(files) > 0:
        print(f"Removing files: {files}")
        c.run(f"dvc remove {files} --outs")
    else: 
        print("Nothing to remove!")
        
def recursive_delete_if_empty(path):
    """Recursively delete empty directories; return True
    if everything was deleted.
    Source: https://stackoverflow.com/questions/26774892/how-to-find-recursively-empty-directories-in-python 
    """

    if not os.path.isdir(path):
        # If you also want to delete some files like desktop.ini, check
        # for that here, and return True if you delete them.
        return False

    # Note that the list comprehension here is necessary, a
    # generator expression would shortcut and we don't want that!
    if all([recursive_delete_if_empty(os.path.join(path, filename))
            for filename in os.listdir(path)]):
        # Either there was nothing here or it was all deleted
        os.rmdir(path)
        print(path)
        return True
    else:
        return False
    
    
def sim_tracked_objects(git_repo, sim_dir): 
    files = []
    for entry in git_repo.commit().tree.traverse():
        if entry.path.startswith(sim_dir):
            files.append(entry.path)
    return files


def find_sim_folders(git_entries):
    
    sim_folders = [] 
    
    for path in git_entries: 

        # Check only dits paths 
        p = Path(path)
        if p.is_dir():
            dir_name = path.split('/')[-1:][0]
            # print(dir_name)
            
            # Dir name may contain only digits and dots
            number_pattern = '^[\.0-9]*$'
            if re.match(number_pattern, dir_name): 
                sim_folders.append(path)

    return sim_folders 


def remove_sim_folder(paths):
    # Check if dir is empty and delete it 
    
    empty_folders = []
    
    for path in paths:
        files = [x for x in Path(path).rglob('*') if x.is_file() & (x.name != '.gitignore')]
        if len(files) == 0: 
            shutil.rmtree(path)
            empty_folders.append(path)

    print(f"Delete empty folders: {empty_folders}")
    
    return empty_folders
        

if __name__ == '__main__':
    
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--stage', dest='stage', required=True)
    args_parser.add_argument('--sim_dir', dest='sim_dir', required=True)
    args = args_parser.parse_args()
    
    outs_dir = Path(f'outputs/{args.stage}')
    
    print("\nDVC_OUTS_REMOVE: Remove files from DVC version control")
    dvc_remove(outs_dir)
    
    print("\nSIM_EMPTY_DIRS_REMOVE: Remove empty dirs from previous simulations")
    git_repo = git.Repo('.')
    git_entries = sim_tracked_objects(git_repo, args.sim_dir)
    sim_folders  = find_sim_folders(git_entries)
    remove_sim_folder(sim_folders)
    
    # Remove empty folders without files 
    recursive_delete_if_empty(args.sim_dir)
    recursive_delete_if_empty(f'outputs/{args.stage}')