import dvc.api
import matplotlib.pyplot as plt
import pandas as pd
from typing import Dict 


def plotFloatPressure(params: Dict) -> None:
    """Analyze CFD simulation results and save plots
    Args:
        params {Dir}: parameters for the stage
    """
    
    # Load params 
    params = dvc.api.params_show()
    project_dir = params['project_dir']
    post_process_dir = params['post_process_dir']
    data_file_path = params['plotFloatPressure']['data_file']
    
    # Read post-processing data 
    PRESSURE_DAT_PATH = f"{project_dir}/{post_process_dir}/{data_file_path}"
    print(f'Pressure data file path: {PRESSURE_DAT_PATH}')
    datContent = [i.strip().split() for i in open(PRESSURE_DAT_PATH).readlines()]
    
    # Process data 
    column_names = ['Time', 'sum(phi)']
    frames = datContent[5:]
    frames[0] = [0, 0]
    df = pd.DataFrame(data = frames, columns=column_names).astype('float')

    # Build & save plot 
    PRESSURE_PLOT_PATH = f"{project_dir}/{post_process_dir}/{params['plotFloatPressure']['plot_pressure_file']}"
    fig, axs = plt.subplots(figsize=(12, 4))       
    df.plot('Time', ax=axs)
    fig.savefig(PRESSURE_PLOT_PATH)
    print(f'Save pressure plot to: {PRESSURE_DAT_PATH}')


if __name__ == '__main__':
    
    params = dvc.api.params_show()
    plotFloatPressure(params)
