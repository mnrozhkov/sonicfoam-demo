import dvc.api
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import shutil
from typing import Dict 


def update_controlDict(f, new_params):
    # type(f): PyFoam.RunDictionary.ParsedParameterFile.ParsedParameterFile 
    
    print(new_params)
    for k in new_params.keys():
        f[k] = new_params[k]
        
    return f

def configureSim(params: Dict) -> None:
    """Plot simulation mesh as PNG file
    Args:
        params {Dir}: parameters for the stage
    """
    
    # Load params 
    params = dvc.api.params_show()
    PATH_PROJECT_DIR = params['project_dir']
    PATH_CONFIGS = params['configureSim']['sim_config_dir']
    PATH_CONTROL_DICT = params['configureSim']['controlDict']['path']
    new_params = params['configureSim']['controlDict']['params']
    
    # Copy config template 
    template_config = f"{PATH_PROJECT_DIR}/{PATH_CONFIGS}/{PATH_CONTROL_DICT}"
    new_config = f"{PATH_PROJECT_DIR}/{PATH_CONTROL_DICT}"

    try:
        shutil.copy(template_config, new_config)
        print("File copied successfully.")
    
    # If source and destination are same
    except shutil.SameFileError:
        print("Source and destination represents the same file.")
    
    # If there is any permission issue
    except PermissionError:
        print("Permission denied.")
    
    # For other errors
    except:
        print("Error occurred while copying file.")
    
    # Update config
    f=ParsedParameterFile(new_config)
    f = update_controlDict(f, new_params)
    f.writeFile()

if __name__ == '__main__':
    
    params = dvc.api.params_show()
    configureSim(params)
